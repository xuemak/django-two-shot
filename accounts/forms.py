# create a login and signup form that is not based on a Django model
# similar to creating a model

from django import forms


class LoginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150, widget=forms.PasswordInput
    )  # the widget argument overrides the form element <input type="text"> that comes in CharField


class SignUpForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput)
    password_confirmation = forms.CharField(
        max_length=150, widget=forms.PasswordInput
    )
