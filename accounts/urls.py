from django.urls import path
from accounts.views import login_user, logout_user, show_signup_form

urlpatterns = [
    path("login/", login_user, name="login"),
    path("logout/", logout_user, name="logout"),
    path("signup/", show_signup_form, name="signup"),
]
