from django.shortcuts import render, redirect
from accounts.forms import LoginForm, SignUpForm
from django.contrib.auth import authenticate, login, logout
from django.http.request import HttpRequest
from django.http.response import HttpResponse
from django.contrib.auth.models import User


# Create your views here.
# login form view POST and GET
def login_user(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":  # will try to create the user and log them in
        form = LoginForm(request.POST)
        if form.is_valid():
            # regular forms dont have a save method to use, so 'cleaned_data' dictionary will get the values for different inputs from the field data
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

            user = authenticate(
                request,
                username=username,
                password=password,
            )
            if user is not None:
                login(request, user)  # logs an account in
                return redirect("home")
    else:  # the GET request will show the form
        form = LoginForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


# logs a person out then redirects them to the URL path registration name "login"
def logout_user(request: HttpRequest) -> HttpResponse:
    logout(request)
    return redirect("login")


# shows the signup form and handle submissions
def show_signup_form(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            if password == password_confirmation:
                user = User.objects.create_user(  # create_user method creates a new user account with the defined parameters
                    username=username, password=password
                )

                login(request, user)
                return redirect("home")
            else:  # If the password and password_confirmation do not match, then the User should not be created and there should be an error that reads "the passwords do not match"
                form.add_error("password", "The passwords do not match")
    else:
        form = SignUpForm()
    context = {"form": form}
    return render(request, "accounts/signup.html", context)
