from django.db import models
from django.conf import settings


# Create your models here.
class ExpenseCategory(
    models.Model
):  # a value that we can apply to receipts like "gas" or "entertainment"
    name = models.CharField(max_length=50)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="categories",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name


class Account(
    models.Model
):  # the way that we paid for it, ie specific credit card or a bank account
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=20)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="accounts",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name


class Receipt(
    models.Model
):  # the receipts app keeps track of this model for accounting purposes
    vendor = models.CharField(max_length=200)
    total = models.DecimalField(decimal_places=3, max_digits=10)
    tax = models.DecimalField(
        decimal_places=3, max_digits=10, null=True
    )  # null=True to allow this property to be historically empty because i previously made migrations on accident before adding the tax property
    date = models.DateTimeField(auto_now_add=False, auto_now=False)
    purchaser = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="receipts",
        on_delete=models.CASCADE,
    )
    category = models.ForeignKey(
        ExpenseCategory,
        related_name="receipts",
        on_delete=models.CASCADE,
    )
    account = models.ForeignKey(
        Account,
        related_name="receipts",
        on_delete=models.CASCADE,
        null=True,
    )
