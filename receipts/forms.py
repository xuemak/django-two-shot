from django.forms import ModelForm
from receipts.models import Receipt, ExpenseCategory, Account


class CreateReceiptForm(
    ModelForm
):  # define a Django model form class by writing our own class that inherits from the django.forms.ModelForm class
    class Meta:  # create an "inner class" named Meta, which is just a class inside a class
        model = Receipt  # specify which Django model it should work with
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        ]


class CreateCategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
            "name",
        ]


class CreateAccountForm(ModelForm):
    class Meta:
        model = Account
        fields = [
            "name",
            "number",
        ]
