from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.http.request import HttpRequest
from django.http.response import HttpResponse
from django.contrib.auth.decorators import login_required
from receipts.forms import (
    CreateReceiptForm,
    CreateCategoryForm,
    CreateAccountForm,
)


# Create your views here.
@login_required  # protects view so that logged in users can access it
def show_all_receipt(request: HttpRequest) -> HttpResponse:
    all_receipts = Receipt.objects.filter(
        purchaser=request.user
    )  # initially was objects.all(). changed the queryset of the view to filter Receipt objects where purchaser = the logged in user
    context = {"all_receipts": all_receipts}
    return render(request, "receipts/myreceipts.html", context)


@login_required
def create_receipt(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        form = CreateReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(
                False
            )  # Have the form save the recipe, but not to the database
            # if all goes well, you can redirect the browser to another page and leave the function
            receipt.purchaser = (
                request.user
            )  # the purchaser must be set to the current user, accessed in request.user
            receipt.save()  # save the recipe using the save method because you are using a ModelForm
            return redirect("home")
    else:
        form = CreateReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_receipt.html", context)


@login_required
def category_list(request: HttpRequest) -> HttpResponse:
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {"categories": categories}
    return render(request, "receipts/expense_categories.html", context)


@login_required
def account_list(request: HttpRequest) -> HttpResponse:
    accounts = Account.objects.filter(owner=request.user)
    context = {"accounts": accounts}
    return render(request, "receipts/account_list.html", context)


@login_required
def create_category(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        form = CreateCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = CreateCategoryForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        form = CreateAccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = CreateAccountForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_account.html", context)
